# JeeSite 4.0 企业信息化快速开发平台

## 项目地址

https://gitee.com/thinkgem/jeesite4-cms

## 配套视频教程

https://ke.qq.com/course/376588?tuin=c55da0ad

视频教程QQ群1：866607936


## Beetl 模板文档

http://ibeetl.com/guide/#/beetl/basic

## JeeSite-module-cms UI

https://adminlte.io/
https://www.pikeadmin.com/demo/charts.html

## JeeSite-module-cms Bug
01. sql bug。
mssql：
Reserved word is used as the column name. Table name : js_cms_site 。
Reserved word is used as the column name. Table name : js_cms_article。
postgresql：
Reserved word is used as the column name. Table name : js_cms_guestbook。


## SiteMesh 3.0.1
http://wiki.sitemesh.org/wiki/display/sitemesh3/Getting+Started+with+SiteMesh+3
## Bootstrap UI  
MIT license

Themes for Bootstrap https://bootswatch.com
https://bootswatch.com/